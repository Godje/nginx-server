#!/bin/bash

# Bash script. Will work on Pine thing. Except alerts will have to be done differently. Through the web process or something

RUN_DIR="/Users/lifeofvictorychurch/.danilo/nginx-server";
NGINX_CONFIG="/Users/lifeofvictorychurch/.danilo/nginx-server/nginx.conf";

function is_running {
	PS_FILTERED=$( ps aux | grep "nginx: master process" -c );
	if [ $PS_FILTERED -gt 1 ]; then
		echo "Yes";
	else
		echo "No";
	fi;
}

cd $RUN_DIR;

IS_RUNNING=$( is_running );
if [[ $IS_RUNNING == "Yes" ]]; then
	echo "NGINX is already running";
	echo "You want me to turn it off?";
	OFF_PROMPT=$( sh ./appleDialog.sh "up" );
	echo "Answer:" $OFF_PROMPT;
	if [ $OFF_PROMPT == "Yes" ]; then
		echo "Sending stop signal";
		osascript -e "do shell script \"sudo nginx -s stop\" with administrator privileges"
		server_running=$( is_running );
		if [ $server_running == "No" ]; then
			sh ./appleDialog.sh "success_off";
			killall stunnel
		else
			sh ./appleDialog.sh "failure"
		fi
	fi
else
	echo "NGINX server is not running";
	echo "You want me to turn it on?";
	ON_PROMPT=$( sh ./appleDialog.sh "down" );
	echo "Answer:" $ON_PROMPT;
	if [ $ON_PROMPT == "No" ]; then
		echo "Server not turned on";
	else
		echo "Starting NGINX server";
		echo "Config file:" $NGINX_CONFIG;
		osascript -e "do shell script \"sudo nginx -c /Users/lifeofvictorychurch/.danilo/nginx-server/nginx.conf\" with administrator privileges";
		did_succeed=$( is_running );
		if [ $did_succeed == "Yes" ]; then
			sh ./appleDialog.sh "success_on"
			stunnel
		else
			sh ./appleDialog.sh "failure"
		fi
	fi
fi

exit

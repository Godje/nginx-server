#!/bin/bash

ffmpeg \
	-threads 3 \
	-ignore_unknown \
	-i rtmp://localhost/1080/test \
	-vcodec libx264 \
	-preset veryfast \
	-x264opts nal-hrd=cbr:force-cfr=1:keyint=15 \
	-b:v 2500k -maxrate 2500k -bufsize 3000k \
	-s 1280x720 \
	-sws_flags spline \
	-r 30 \
	-acodec copy \
	-f flv rtmp://localhost/720/test

#!/bin/sh

#Bruh. Script for MAC. It's not going to work on the Pine thing

if [ $1 == "up" ]; then
	osascript -e '
		display dialog "The server is up, you want to turn it off?" buttons {"Yes", "No"} default button 1
		if button returned of result = "No" then
			do shell script "echo No"
		else
			if button returned of result = "Yes" then
				do shell script "echo Yes"
			end if 
		end if
	'
elif [ $1 == "down" ]; then
	osascript -e '
		display dialog "The server is down, you want to turn it on?" buttons {"Yes", "No"} default button 1
		if button returned of result = "No" then
			do shell script "echo No"
		else
			if button returned of result = "Yes" then
				do shell script "echo Yes"
			end if 
		end if
	'
elif [ $1 == "success_on" ]; then
	osascript -e '
		display dialog "The server is now on." buttons {"Ok"} default button 1
	'
elif [ $1 == "success_off" ]; then
	osascript -e '
		display dialog "The server is now off." buttons {"Ok"} default button 1
	'
elif [ $1 == "downscale_on" ]; then
	osascript -e '
		display dialog "Facebook Downscaling is now running" buttons {"Ok"} default button 1
	'
elif [ $1 == "failure" ]; then
	osascript -e '
		display dialog "Server state did not change. Danik messed the script up or something" buttons {"Ok"} default button 1
	'
fi

#!/bin/bash

SESSION_NAME="downscaler"
IS_RUNNING=$( tmux list-sessions | grep "downscaler" -c );

if [ $IS_RUNNING -gt 0 ]; then
	echo "server is already running";
else
	cd /Users/lifeofvictorychurch/.danilo/nginx-server/
	# Creating a session

	tmux new-session -d -s $SESSION_NAME
	tmux select-window -t $SESSION_NAME:0
	tmux send-keys "sh ./ffmpeg_start.sh" C-m

	tmux new-window -t $SESSION_NAME:1 -n "Bruh"
	tmux select-window -t $SESSION_NAME:1
	tmux send-keys "sh ./appleDialog.sh downscale_on" C-m
fi

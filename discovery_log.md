# Nginx Project Log

## General info

Most of the info that I got from the internet was from the obsproject.com website. Their forums have good information about streaming in general, and people in comments seem to be experienced with what is happening. They get good explanations, and sometimes I will copy them here just for future reference.


### 02-26-2018

So there's basically a problem with the drops of bitrate so I started making many research about it. People be having problems with it usually point at their connection. Nobody states the problem as "my bitrate drops to 0", but "my stream freezes for a second", which is clearly not what is happening to us, but I read all of those answers nonetheless.
Many people suggested that this is the connection that sucks, not the encoding or anything. Some people reported their speedtest.net results and peeps responded that speedtest is not accurate enough for the service we are trying to pull off. More later in this answer from a guy:

#### - About speed test
<!-- Answer from:
https://obsproject.com/forum/threads/every-few-seconds-my-stream-freezes-for-a-second-or-two.6022/#post-33102
-->
```
Speedtest tests 'dead file' transfers. Useless for livestreaming, which relies on a constant throughput. Many ISPs will 'burst' connections (normally run at a low throughput level, then run FAR above their rated speed briefly now and then) which will average out to the speed they say they're giving you over the course of a file transfer. Unfortunately, livestreaming only has a certain amount of data available at a given time, so cannot take advantage of these bursts of super high bandwidth.
```

Also, mostly all of the questions and answers were about Twitch streaming, which is different from Facebook streaming. Some people resolved the issue with doing what answer said to do (probs not gonna work for us, I can see), or just changing the server they are streaming to (which we can't do with Facebook. Facebook is anus).

#### - Stream the stream?

I think that we will have to keep Facebook. All of the non-facebook solution won't allow peeps on facebook to re-stream our thing. And this is most likely one of the features that Triska cares about because all of the friends of the people who re-stream are seeing it not as a repost as those friends actually streaming, when it's just duplicating our stream. Something like that.



### 06-03-2018

I started looking up the issue of the green screen showing up sometimes. And it's very hard to find the answer because I don't really know how to word it. We can kind pull off some deduction, that this is a decoder in the browser, or the encoder issue. There was an answer that was very useful to get what I know right now. And the problem might as well be with the fact that we're testing on Safari.

#### Possibility that the problem is with Safari
<!-- Answer from:
https://github.com/video-dev/hls.js/issues/1003#issuecomment-282823950
-->
	Green artifacts usually mean a problem with the encoded stream or a bug in the video decoder. Firefox and Safari use macOS' H.264 system decoders, but Chrome bundles its own H.264 decoders. That's probably why you don't see a problem in Chrome. When running Firefox from the terminal, I see the following error messages on stderr:

	```
	GVA error: scheduleDecodeFrame kVTVideoDecoderBadDataErr, error in field pic flags
	GVA error: scheduleDecodeFrame kVTVideoDecoderBadDataErr, error in field pic flags
	GVA error: scheduleDecodeFrame kVTVideoDecoderBadDataErr, error in field pic flags
	GVA error: scheduleDecodeFrame kVTVideoDecoderBadDataErr, error in field pic flags
	```

We should investigate from other servers if the green problem perseveers. If that is the case, then we should deal with an encoder. I have a somewhat of a plan on how to test where the problem is.

1. We are going to stream and see the stream from VLC and see if the green thing is coming up.
	-	if it does, that is not a decoder issue, because multiple decoders have the same problem
2. We are going to record the video locally on a drive, using the same codec. Because Wirecast would take care of recording, not the RTMP server, we will see if the problem is on Wirecast's side of encoding the video.
	-	if the video has the green part in it, then it's Wirecast's encoding issue, and we should fix the encoding issue.
3. If that is not the case, and the green part doesn't come up on a recording, we will try recording with an RTMP server, and see if the green comes up there.
	-	if the green comes up there, but not in Wirecast's recording, then it is Nginx's part, or RTMP module's part of things that ruins this endevour.

Fixing the last problem would mean we have to encode/recode the stream on the Nginx server's application. FFMPEG or Avcodec or something of that sort, and see if there's the problem.
It all comes down to infinite testing so far, because we don't know for sure where the problem resides.

The whole issue on github is pretty interesting. Although it is not about RMTP server at all, it is about live streaming, and the guy specifically describes the problem as **Green Screen in Firefox and Safari sometimes**
<!-- Answer from:
https://github.com/video-dev/hls.js/issues/1003#issue-210454359
-->
The thing is, is that the issue was published in February 2017, and both Firefox and Safari should have most likely received many reports from other devs and fixed the issue. It has been 2 years. It might as well be that we didn't update Safari, and we have this weird issue.

### 03-07-2019

Ok so basically I have tested stuff on my dad's computer, and there hasn't been any green thing dashing through the video. As I am typing this it has been 4 minutes of me running the stream to YouTube only and looking at it from my dad's windows computer with Edge browser, not even Chrome, and there has not been an issue in the video yet. Yes, the quality is the buffering style quality, you really can't do much about it. We would have to step up the bitrate and change the keyframes, and it will be fine. It will also work out if we port redistribution to a separate machine.

So basically, the only thing that is left for us to configure is the facebook part of things. Since we can run the stream, but we can't start it from just the button. We also can't end it through the button. I am legit out of ideas on how to make RTMP server understand that the input is dropped and the call to the Facebook should be made to close the stream. Or make RTMP understand that the thing is running and it should send the start of the stream command.

Yes, the green issue is resolved. It has been 14 minutes running and I haevn't seen a bad frame yet.
It is the unupdated browser issue that we have going on on the Mac

Now to the quality side, I guess we would have to flip around encoders and see which one is going to work best. Setting the encoder to 30frames keyframe is not helping much. Setting the bitrate 1000 more is not helping because it is not constant bitrate, it is variable. So I will be testing some settings and telling you how it is.

1. Average Bitrate: 3000. 30 keyframes
	- the quality is barely better. More pleasant to an eye that before, but sucks if you would ask literally anyone.
	- CPU usage: 60% average
2. Average Bitrate: 3000. every single frame is a keyframe
	-	this is disgusting. In theory, if keyframes send new info about the frame to update the frames with new info and make it look better. But in practice, it looks ugly, because the stream got no time to make use of those key frames andeverything legit look 480p
	- CPU usage is even lower. Like 30%, with every key as a keyframe.
	- The livestream starts buffering and loggin after 2 seconds of viewing. 
	- Just a bad option in general
3. Avg Bitrate:3000. Every 30 frame is a keyframe. Apple H264 encoding
	-	quality is anus
	- CPU is not bothered at all

In all honesty, I don't know what kind of expectations should I set for this.
It's running 1080p Default Wirecast settings now, and it has slightly better quality. You wouldn't call it picture-like any time. Yeah it is better some way or another, but 9 feet away I wouldn't see a difference at all.

And 1080p be dropping frames crazy. Its been 8 minutes and we dropped almost 800 frames. That is on 1080p Default Wirecast settings. I don't feel like it will change big time when we move the thing to a separate machine. Overall, 1080p streaming is almost a no-no on the machine we have at the moment. This setup is kind of an overkill for this computer.

Also, it doens't look like a CPU load is a problem in the case of 1080p streaming. Because the CPU load is 80% but the frames are being dropped for real. Dropped 1000 frames in 9.5 minutes.

### 03-24-2019

The new major discovery and sad life approaching.

Ok, so basically, this involves actual legit programming. I have figured out **exactly** how the Wirecast is streaming to Facebook without the "Go Live" button.
This can take time to explain, and I will, and please read this.

So, I started looking into Facebook API. Looked into what access-token is and how to get it, so that you can use it in the "curl" command when posting the stream, which is exactly how it works (RTMP has built-in control module modification).

I would basically have to make a custom, scratch-made Application. I can use just bash commands, ncurses interface, web interface, whatever I want, but it still is an application on the bottom, which requires its own API token (it's fine, I bet we can get one).

To make Triska and the rest of the squad use the stream APP I would have to build my own web interface on top of the program I make with Manual Login Flow, and the rest of the good stuff. I would either have to use Apache + PHP which I never knew, or use Node.js and JavaScript to power this whole thing. The only concern I have is that the processes ran by Node.js have to be multi-core, while Node.js is single-threaded. I would have to simulate curl commands with Node.js, that will run on localhost NGINX server



### 04-18-2019

Project resurrected because we updated the MacOS and the Wirecast just went sicko mode. Now we are forced to use OBS, which means we are forced to use NGINX, so we have to use separation of RTMP signal. We figured out the settings that are OK for both streams and we are running at 10% of CPU usage for 30fps 720p stream no problem. Also, instead of Node.js server and a C++ program we are running a BASH script that starts the server. Will have to be changed eventually, because we have to figure out downscaling.

Everything is working so for but we just made do with the "Go Live" button. You can't avoid it and it yeah, life.

Now we can figure out how to downscale the video. It has to be a separate running process of ffmpeg, that converts the audio stream. So it's a whole separate program that does this. NGINX is only going to have two rtmp server applications, one general and one for Facebook that FFMPEG is going to use.

This is one way of implementing this: 
[link](https://helping-squad.com/ffmpeg-transcode-copy-flv-to-mp4-and-more/)

If downscaling is going to impact the CPU, we will have to move the whole thing to a separate server box, which is fun. Also, since downscaling is a separate program, we will have to figure out a normie way of starting the program, so that it downscales. We can't have two scripts, that someone will have to start. That's too much for a normie brain to figure out or learn. People will always forget and Facebook streaming will get broken half the time because we can't push 1080p there. So, basically, if the FFMPEG affects CPU, which it won't, but for some reason I hope it does, we can move the thing to a different machine and start all the processes automatically, including FFMPEG conversion. We won't have to write normie programs in BASH like freaks, and have some *ok* documentation for peeps here to read and understand how the code works. It's still going to run automatically, since Linux has cron-jobs and @at scheduling. We can run a checker automatically, and if the server is off, or sending 1/2 signals, we can restart it with a bash script.

I don't actually know what 1/2 signal is, It's error codes. Basically, the script output is usually 0, if it was executed without errors. If server starts sending error code, it means it sends $? == 2 signals. That's what I meant by 1/2 signals

### 04-21-2019

So, I think there is a way to implement an FFMPEG without a running session of terminal that will confuse all the normies.

I can write a TMUX script, that will run the command, and then detach itself from the command line and exit. The only thing that will be a big hassle, is the dialogs that say "Oh yeah we are downscaling successfully" and everything. Those are the ones that will take more time than the autonomous script. I have to learn a little bit of AppleScript to run the script without Shell conditional statements and assign variables myself. That way it will be more efficient (not that script efficiency matters at all), and more maintainable, because we can have functions with that thing.

After so many tryings that we had last time, it makes no sense to try again, assigning Bash variables into AppleScript. We would have to write a separate AppleScript file that will process triggers and so on. Except that will basically be a "re-learning Bash" kind of experience. Functions, conditions, echo's and argument processing and all of that nonsense...

10 minutes later. I give up. This is pointless. AppleScript is literally nonsense. Bash Scripting, and that's it. Not going any further than this. No matter how big of a mess our "appleDialog" file becomes, it's all Apple's fault for creating this nonsense of a scripting language. 

### 04-23-2019

So, I have foudn some kind of control module for RTMP. Which is a separate server, probably running on Node.js. Also am setting up the stat.xsl for the server. I had to do it earlier, but only got to it now, because we finished all of the first-level priority stuff, like optimizing the process for normies.
The monitoring server I found doesn't work. Most likely we will be able to set up on a separate box, because it will run Linux, and not have 1 million watching files. Hopefully, it doesn't take up CPU either. But we just have to hope. For now I am going to use the default stat.xsl stuff that documentation provides. Just have to reload the server after the service ends so that we don't break anything

### 04-28-2019

Ok, so basically, we need the streaming box. Otherwise, the normies are going to break the stream. Other than that, I will set up a web-scraping server that will get the rtmp server url and the key, and automatically update the nginx.conf by command. I can do node.js server for that, as long as we have a process assigned for it.

Also, if we are going to be able to run a Node.js server with stuff, I might as well set up a DIY server panel, where you can add as many servers as you want. Or manually write the freaking file, from browser, update, and start the server.

Weird problem happened today. Guys on booth said the audio disappeared, so they, like all the normies, just messed everything up and changed the persistent streaming key, forcing me to re-set up the server, and restart YouTube stream as well. Basically, nobody, but people who know what streaming is to the core (George, Daniel, Taras), should have any access to Facebook's controls, because they mess everything up.

So, basically I started doing the web-scraping, web-interface and web-everything in node.js

### 05-01-2019

The progress is halted at this point. We need to set up stunnel. Or "Secure Tunnel". Basically a TSL/SSL connection with the RMTP server IP that Facebook gives us, and then stream to localhost. Basically poop style. There's a video about it on Windows, but Stunnel can run headless on an Ubuntu server as well.
[video](https://www.youtube.com/watch?v=e6JeRVI0ioQ). 
He also has an nginx.conf config. So we can do that as well. Web-scraping won't be necessary at this point. Facebook says that if RMTPs is set up, there's no need to click "GoLive", since it is a secure connection lol. It's weird and I don't understand their shtick, but life

### 05-05-2019

So, I managed to set up the Secure Tunnel thing. The video from previous day literally was the only thing I needed. I compiled stunnel from source and ran the config that video suggests and it fixed it. We can now stream to facebook yet again. I am just going to put the checker of stunnel into `runscript`. Also, unlike `nginx`, `stunnel` doesn't need root rights to be running. So there's barely going to be errors in running stunnel. We don't have to request additional rights. We managed to finish our practice quickly so I had time to set the thing up. I am going to automate it during the service, and come back to trying to figure out web-scraping and clicking on "Live" button automatically.

### 05-08-2019

So I watched some Express.js crash course online. The thing is, we don't really need for Node.js/express js to handle the routes. I can use my skills in Mithril.js to build a routing strategy with views and everything. Except, in this case it will be basically impossible for anyone but a JavaScript expert to manage the views. So I am going to keep Express handling routes. Except now I realised we don't really need a Database library. Express.js has a very good way of handling get requests. We can just assign different /api/"method" routes to get whatever we need. So:

```javascript
app.get("/api/keys", function(req, res){
		res.json({
				"keys": [
					{"id": 0}
				]
				});
		});
```

Express JS can just send a local json file as well. Or, we can still use our database, just not send the stuff through Jade (Pug) thing, but through a JavaScript request. That way, on a "post" request, we can just `res.json({"success": "true"})`, and JS is goign to take care of having a little green text "Saved!" by the button

Basically, Express.js is pretty cool. With this in mind I am going to rewrite a good bit of our thing in church today (Wednesday).Going to keep on logging there, but I am sending this to you right now.

## 05-12-2019

Managed to set up a DB update, that responds with redirecting back to the page that the POST was sent from. JavaScript handles the GET request to see if the process actually went through correctly, it is not Express.js that renders a "Success" message, but a response from Express.js that I call for every time the Settings page is opened. Not the most optimized thing, but in all honesty I don't really bother with optimization in our case. Everything is on such a small, unambitious scale, that I don't have to bother about future scaling. This is literally a crutch fix for a custom RTMP server

## 05-19-2019

Node.js docs are immensely useful. Basically, by default the process of invoking a command in a shell is asynchronous. What that means is that the Node.js spawns a process that doesn't block an event loop. It runs itself "in the background", and after it's complete or during it's process it will run a callback function that you pass into it. Well, in our case, since we are toggling the server with a switch, and the **client** waits for the response from the **server**, we will most likely have to implement a **synchronous** instead of an **asynchronous** shell runner.

Node.js provides those too, *more than that* (!), Node.js does not discourage the usage of synchronous scripts if they have purpose:

```
For certain use cases, such as automating shell scripts, the synchronous counterparts may be more convenient. In many cases, however, the synchronous methods can have significant impact on performance due to stalling the event loop while spawned processes complete.
```

We are indeed automating shell scripts here, and it will be more convenient to just halt until completion, so we're good and everything is going to be just fine if we halt the server for the execution. We only have one client connected to the server at any point in time, so halting a server for running a server is going to be ok.

Also, i tried running an example LS command and stuff. It does log out and spawns the process just fine here on Mac. I bet there will be no problem on Linux

## 05-26-2019

I am basically blind-writing code because there is stuff i legitimately can't test yet even I were to turn off the stream. I am not writing the shell automation yet, because well, we are no on Linux file-system yet and I don't have much time after church to test if PIDs can be returned, if errors are piped into an execution stream and so on.

I am currently debating should I rewrite half of the main javascript file to fit the FileAsync, so that `lowbd` is running asynchronously. It allows a nice `db.write().then( fn )` stuff. That way, you are sure that the write was succesful, and the message is going to ship right after it's done. In the case that we have now, the script coming after the `db.write()` is executed only after the function `db.write()` is complete, but looks trashy.

So the whole commotion of me changin the scrript to Async is just for looks. Readability. I don't know if I should do that.

## 06-26-2019

All the systems are working. Saving to the database, reloading the server, turning off Node without NGINX. I am not going to rewrite for Async of course. The shell scripts are running correctly and everything works. 
Also execution of scripts is running from the same folder that the node.js is running. So I don't have to `cd` into the working directory. I also don't have to `../PASSWORD`, since it's in the same folder than node.js is running. I made a mistake of `../P` before and the scripts was broken. Now I don't have to input the password, and no one will have to, from the normie side.

The only thing that is left, and is quite low priority, is to have automatic "Go Live" on Facebook. That way we'll demolish Wirecast. Cause OBS is easier on our weak computer, and we can stream 1080p, AND without clicking Go Live, WITHOUT "Creating" a dynamic RTMP url.

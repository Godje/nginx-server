'use strict';

let switchEl = document.querySelector(".switch");
let stunnelEl = document.querySelector(".stunnel-reload");
let stunnelStatus = document.querySelector(".stunnel-response");

//getting the switch's actual state
getSwitchState();

// Click event for Switch's element
switchEl.addEventListener("click", function(e){
	let body = e.target;
	sendPost(
		"/api/toggleState", 
		( switchEl.className.indexOf("left") > -1 ?
			"buttonState=on" : "buttonState=off" ), 
		function( response ){
			let data = JSON.parse(response);
			data.toggleState == "toggled" ?
				getSwitchState() : void 0;
		});
});

stunnelEl.addEventListener("click", function(e){
	let body = e.target
	sendPost(
		"/api/reloadStunnel",
		null, function(response){
			let data = JSON.parse(response);
			data.success == true ? 
				stunnelStatus.textContent = "success" :
				stunnelStatus.textContent = "failure";
		}
	)
});

// shortcut for sending a post request. Made it so in the future if we send more than one it won't go messy
function sendGet(url, param, callback){ 
	const Http = new XMLHttpRequest();
	Http.open("GET", url, true);
	Http.send();
	Http.onreadystatechange = function(){
		if( this.readyState == 4 && this.status == 200 ) 
			callback( this.responseText );
	}
}
function sendPost(url, param, callback){
	const Http = new XMLHttpRequest();
	Http.open("POST", url, true);
	Http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	Http.send(param);
	Http.onreadystatechange = function(){
		if( this.readyState == 4 && this.status == 200 ) 
			callback ? 
				callback( this.responseText ) 
			: console.log( this.responseText );
	}
}

function getSwitchState(){
	sendGet("/api/nginxState", null, function(response){
		let data = JSON.parse( response );
		if( data.nginxState === true ) 
			switchEl.className = "switch left"; 
		else if ( data.nginxState === false ) 
			switchEl.className = "switch right"; 
		else 
			switchEl.className = "switch right";
	});
}


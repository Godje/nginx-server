function $(str){
	let els = document.querySelectorAll(str);
	if(els.length == 0) return;
	return els;
}

function closeDropdowns(){
	$(".dropdown").forEach(function(dropdown){
		dropdown.classList.remove("is-active");
	})
}

$(".dropdown").forEach(function(dropdown){
	console.log(dropdown)
	dropdown.addEventListener("click", function(event){
		event.stopPropagation();
		dropdown.classList.toggle("is-active");
	});
});

$(".expand-toggle").forEach(function(button){
	let target = $("#" + button.getAttribute("data-target"));
	button.addEventListener("click", function(event){
		event.preventDefault();
		target[0].classList.toggle("expanded");
	});
})

document.addEventListener("click", function(event){
	closeDropdowns();
});

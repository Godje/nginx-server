'use strict';

function sendGet(url, param, callback){
	const Http = new XMLHttpRequest();
	Http.open("GET", url, true);
	Http.send();
	Http.onreadystatechange = function(){
		if( this.readyState == 4 && this.status == 200 ) 
			callback( this.responseText );
	}
}

sendGet("/api/lastWriteSuccess", null, function(response){
	let domEl = document.querySelector("span.response");
	let json = JSON.parse(response);
	if( json["lastWriteSuccess"] == true ){
		domEl.className = "response success";
		domEl.textContent = "Success. Don't forget to reload Server (NGINX)";
	} else if( json.lastWriteSuccess == false ) {
		domEl.className = "response failure";
		domEl.textContent = "Error";
	} else {
		console.log("no changes recorded");
	}
});

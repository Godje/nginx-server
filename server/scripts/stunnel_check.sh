#!/bin/bash
psOutput=$( ps aux | grep 'stunnel /home/rock64/server/nginx-server/server/scripts/stunnel.conf' -c )
if [ $psOutput -gt 1 ]; then
	echo 'stunnel is running'
	exit 0;
else
	stunnel /home/rock64/server/nginx-server/server/scripts/stunnel.conf
fi

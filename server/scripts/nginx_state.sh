#!/bin/bash
grepOutput=$(ps aux | grep -e '\broot\b.*\bnginx.conf\b' -c)
if [ $grepOutput -eq 1 ]; then
	echo 'running'
else
	echo 'off'
fi

#!/bin/bash
SERVER_DIR="/home/rock64/server/nginx-server/server/"

psOutput=$( ps aux | grep 'node index.js' -c )
if [ $psOutput -gt 1 ]; then
	echo 'node.js is running'
	exit 0;
else
	SESSION_NAME="node.js"
	tmux new-session -t 1 -d
	tmux new-window -t $SESSION_NAME:1 -n "nodejs"
	tmux select-window -t $SESSION_NAME:1
	tmux send-keys "bash" C-m
	tmux send-keys "cd $SERVER_DIR && npm run start" C-m
	tmux rename-window -t $SESSION_NAME:1 "nodejs"
	tmux kill-window -t $SESSION_NAME:0
	exit 0;
fi

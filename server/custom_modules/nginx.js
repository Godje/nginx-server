'use strict';
const directory = "~/server/nginx-server/";
const sessionStorage = require("./sessionStorage.js");
const exec = require("child_process").exec;
const execSync = require("child_process").execSync;
const fs = require("fs");
let db = sessionStorage.db;

function log( text ){
	let date = new Date();
	console.log( `[${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}]`, text);
};

module.exports = {
	on: function(){
		let pushURLs = db.get("keys").value().map(function(el){
			return `# ${el.name} \n push ${el.url}${el.key} ;`;
		});
		if ( sessionStorage.nginxState == true ) { return; }
		else {
			fs.readFile( sessionStorage.nginxTemplateSrc, 'utf8', function(err, data){
				if(err) return console.log(err);
				let result = data.replace('# <++>', pushURLs.join("\n"));
				fs.writeFile( sessionStorage.nginxTargetPath, result, (err) => {
					if(err) throw err;
					console.log('The file has been saved!');
				} )
			});

			let cmdReturn = execSync(
				'sh '+ directory + 'server/scripts/nginx_on.sh', {
				encoding: 'utf-8'
			});

			sessionStorage.nginxState = true;
			return;
		}
	},

	off: function(){
		let cmdReturn = execSync(
			'sh ' + directory + 'server/scripts/nginx_off.sh', {
			encoding: 'utf-8'
		});
		sessionStorage.nginxState = false;
		log("sessionStorage set to false");
	},

	state: function(){
		var upState = sessionStorage.nginxState;
		let cmdReturn = execSync(
			'sh ' + directory + 'server/scripts/nginx_state.sh', {
			encoding: 'utf-8'
		});
		let pass = new RegExp("running", "g");
		upState = pass.test( cmdReturn );

		sessionStorage.nginxState = upState;
		return upState;
	},

	reloadStunnel: function(){
		let cmdReturn = execSync(
			'sh /home/rock64/server/nginx-server/server/scripts/stunnel_restart.sh', {
				encoding: 'utf-8'
		});
	}
}

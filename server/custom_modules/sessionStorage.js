'use strict';

module.exports = {
	db: null,
	lastWriteSuccess: undefined,
	nginxState: false,
	nginxTemplateSrc: "./scripts/nginx.conf.template",
	nginxTargetPath: "./scripts/nginx.conf",
	nginxStart: "./scripts/nginx_on.sh",
	nginxStop: "./scripts/nginx_off.sh",
	stunnelRestart: "./scripts/stunnel_restart.sh"
}

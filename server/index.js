'use strict';
// LOWDB setup
const lowdb = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const adapter = new FileSync("db.json");
const db = lowdb(adapter);
const port = 3000;
const sessionStorage = require("./custom_modules/sessionStorage.js");
sessionStorage.db = db;
const nginx = require("./custom_modules/nginx.js");

// APPLICATION setup
const express = require("express");
const bodyParser = require("body-parser");
const app = express();

// NODE JS libs
const http = require("http").Server(app);
const path = require("path");

// PUPPETEER setup
const puppeteer = require("puppeteer");

// APPLICATION processing
app.set('view engine', 'pug');
app.use( express.static('static') );
app.use( bodyParser.urlencoded({ extended: true }) );

// index page
app.get("/", function(req, res){
	res.render("index", {title: "Hey", message: "Hello there!"})
});
// controller page
app.get("/controller", function(req, res){
	res.render("controller", {
		allowOn: false,
		nginxState: nginx.state()
	});
})
// settings page
app.get("/settings", function(req, res){
	res.render("settings", {
		ytlink: db.get("keys").value()
	});
});

// API
// get
app.get("/api/lastWriteSuccess", function(req, res){
	res.json({
		lastWriteSuccess: sessionStorage.lastWriteSuccess
	});
	sessionStorage.lastWriteSuccess = undefined;
});

app.get("/api/keys", function(req, res){
	res.json({ data:  db.get("keys").value() })
});

app.get("/api/nginxState", function (req, res){
	nginx.state();
	res.json({ nginxState: sessionStorage.nginxState });
});
// post
app.post("/api/reloadStunnel", function(req, res){
	nginx.reloadStunnel();
	res.json({ success: true });
});

app.post( "/api/changeKeys", function(req, res){
	log("[API/post] "+ req.url +" received")
	let data = req.body;
	let objKeys = Object.keys(req.body);
	objKeys.forEach( function(key){
		let tag = key.split("-for-");
		let param = tag[0];
		let id = tag[1];
		db.set(`keys[${ id }][${ param }]`, data[key]).write();
	});
	sessionStorage.lastWriteSuccess = true;
	res.redirect('back')
});
app.post( "/api/toggleState", function (req, res){
	log("[API/post] "+ req.url +" received")
	let data = req.body;
	if( data.buttonState == "on" ){
		nginx.off();
	} else if ( data.buttonState == "off" ) {
		nginx.on();
	} else {
		console.log(nginx.state(), "button state undefined. Can't toggle")
	}
	res.json({ "toggleState": "toggled" })
});

// starting the http (port 3000) server
http.listen( port, () => console.log(`Example app listening on port ${port}!`) );

// LOWDB processing. It just works so far. Set for ignore in the nodemon. Otherwise reboots all the timw
db.defaults({ keys: [] })
	.write();
// HELPER FUNCTIONS
function log( text ){
	let date = new Date();
	console.log( `[${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}]`, text);
};

// LINKS USED
//
// POST call:
// https://stackoverflow.com/a/15569215/5410502
// NODE.js child process:
// https://nodejs.org/api/child_process.html#child_process_child_process

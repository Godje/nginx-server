# nginx-server

This repository is church's repository for the NGINX RTMP streaming server box. Dotfiles will be here. Scripts will be here. It will get more organized later, as soon as we move to the actual box. `.bashrc` and so on will come then.

**OS**: Linux, Ubuntu

**Dependencies**: node.js (8.12.0+), stunnel, nginx (built with rtmp module), bash

Node.js folder is `server`. Default port is 3000. NGINX is going to assign port 80, to forward to 3000 by default, so you don't have to change the Node.js port to be accessible through just an IP.

Instruction:

```bash
# clone
git clone <this-repo>
cd nginx-server/server

# create a PASSWORD file
echo <root-password> > PASSWORD #it will be used to run the sudo scripts.

# run the server
npm install
npm run start
# if you want to develop with nodemon
npm run dev
```


`server/scripts/*` - the folder contains Shell Scripts that node.js (and crontab) are going to execute. Not planned to be executed manually, but can be, at any stage. Doesn't really ruin anything, just have to reload the page on the client to see the results. Although, it's Node.js's job to see if the server is running already, so if you run scripts manually you can accidentaly turn on nginx twice (which will throw an error, and won't run twice, you're technically safe).

`server/scripts/stunnel_check.sh` - a file that you put into a crontab, to run every few minutes to see if stunnel is running. Solely for the purpose of people accidentaly reloading/turning off the machine, machine turning back on. There's a better way to run on boot, but I just went with this thing anyways.

`server/scripts/node_check.sh` - a file that you put into a crontab, to run every few minutes to see if node js is running. For the same reason as Stunnel check
